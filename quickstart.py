from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
import csv

import configparser

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '14_BInqDzV5q3PxuSM6lEXbI_3UI5SGHp190dUHdBJM0'
SAMPLE_RANGE_NAME = 'Sheet1'

def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('sheets', 'v4', http=creds.authorize(Http()))

    # Call the Sheets API
    SPREADSHEET_ID = '14_BInqDzV5q3PxuSM6lEXbI_3UI5SGHp190dUHdBJM0'
    RANGE_NAME = 'Sheet1'
    result = service.spreadsheets().values().get(spreadsheetId=SPREADSHEET_ID,
                                                range=RANGE_NAME).execute()
    values = result.get('values', [])
    f = open("sheet.csv","w")
    writer = csv.writer(f)
    writer.writerows(values)
    import configparser

    config = configparser.RawConfigParser()


    config.add_section('Section1')
    for val in values:
        print(val)
        config.set('Section1', val[0], val[1])
    
# Writing our configuration file to 'example.cfg'
    with open('example.ini', 'w') as configfile:
        config.write(configfile)
if __name__ == '__main__':
    main()
