import gspread
from oauth2client.service_account import ServiceAccountCredentials
import configparser 
import csv


#Function for Creating INI file for sheet's data:
def createini(list_of_hashes):
    config = configparser.RawConfigParser()
    config.add_section('Property')
    for val in list_of_hashes:
        print(val)
        config.set('Property', val[0], val[1])
    # Writing our configuration file to 'example.cfg'
    with open('out.ini', 'w') as configfile:
        config.write(configfile)


#Function for Creating CSV file for sheet's data:
def createcsv(list_of_hashes):
    f = open("out.csv","w")
    writer = csv.writer(f)
    writer.writerows(list_of_hashes)

#Function for Fetching values from google sheet:
def values(sheet):
    # Extract and print all of the values
    list_of_hashes = sheet.get_all_values()
    #print(list_of_hashes)
    createcsv(list_of_hashes)
    createini(list_of_hashes)



#Function Checking authorization for google sheet with credentials and fetching sheet from drive:
def author():
    # use creds to create a client to interact with the Google Drive API
    scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
    client = gspread.authorize(creds)
    # Find a workbook by name and open the first sheet
    # Make sure you use the right name here.
    sheet = client.open("sheet1").sheet1
    values(sheet)

#Calling author function for authentication:
author()
